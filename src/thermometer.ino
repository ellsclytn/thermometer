#include "env.h"
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <DHT.h>

#define DHTPIN 12
#define DHTTYPE DHT22

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE);

void setupWifi() {
  Serial.print("Attempting WiFi connection");

  delay(10);

  // Disable access point mode
  WiFi.mode(WIFI_STA);

  // Begin connecting to wifi
  if (WiFi.SSID() != WIFI_SSID) {
    WiFi.begin(WIFI_SSID, WIFI_PASS);
    WiFi.persistent(true);
    WiFi.setAutoConnect(true);
    WiFi.setAutoReconnect(true);
  }

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println(" Connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
}

void reconnectPubsub() {
  while(!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    if (client.connect(WiFi.macAddress().c_str(), MQTT_USER, MQTT_PASS)) {
      Serial.println(" Connected.");
    } else {
      Serial.print("Connection failed, error code: ");
      Serial.print(client.state());
      Serial.println(". Will retry in 5 seconds.");

      delay(5000);
    }
  }
}

void setupPubsub() {
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(handleMqtt);
  reconnectPubsub();
}

void handleMqtt(char* topic, byte* payload, unsigned int length) {
}


void setup() {
  Serial.begin(9600);

  dht.begin();
  setupWifi();
  setupPubsub();

  delay(1000);

  float h = dht.readHumidity();
  float t = dht.readTemperature();

  if (!isnan(h) && !isnan(t)) {
    char humidity[6];
    dtostrf(h, 3, 2, humidity);
    char temperature[6];
    dtostrf(t, 3, 2, temperature);

    client.publish("hi", "hello");
    int temperaturePublished = client.publish(TEMPERATURE_TOPIC, temperature);
    int humidityPublished = client.publish(HUMIDITY_TOPIC, humidity);

    if (humidityPublished && temperaturePublished) {
      Serial.print("Sent reading: ");
    } else {
      Serial.print("Failed to send reading: ");
    }
    Serial.print(temperature);
    Serial.print("C, ");
    Serial.print(humidity);
    Serial.println("%");
  }

  ESP.deepSleep(600 * 1000000); // Sleep every 10 min
}

void loop() {
}
